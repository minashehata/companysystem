//
//  Router.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 07/04/2021.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit

class Router {
    class func openCompaniesViewController() -> UINavigationController {
        let companiesController = CompaniesAutoUpdateController()
        companiesController.presenter = CompaniesPresenter(view: companiesController)
        return UINavigationController(rootViewController: companiesController)
    }
    
    
    class func openCompanyViewController(company: Company?) -> UINavigationController {
        let editCompanyController = CreateCompanyController()
        editCompanyController.presenter = CompaniesPresenter(view: editCompanyController, company: company)
        let navCont = UINavigationController(rootViewController: editCompanyController)
        navCont.navigationBar.backgroundColor = .lightRed
        return navCont
    }
 
    
    
    class func transition(with window: UIWindow) {
        let transition = CATransition()
        transition.duration = 0.4
        transition.type = CATransitionType.fade
        window.layer.add(transition, forKey: kCATransition)
    }
    

}
