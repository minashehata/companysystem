//
//  CreateCompanyController.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit

protocol CreateCompanyControllerDelegate {
    func didAddCompany(company: Company)
    func didEditCompany(company: Company)
}

class CreateCompanyController: UIViewController {
    
    var presenter: CompaniesPresenter!
    
    private func setupCircularCompanyImageView() {
        companyImageView.layer.cornerRadius = companyImageView.frame.width / 2
        companyImageView.layer.masksToBounds = true
        companyImageView.layer.borderColor = UIColor.darkBlue.cgColor
        companyImageView.layer.borderWidth = 2
    }
    // not tightky-coupled
//    var delegate: CreateCompanyControllerDelegate?
    
    lazy var companyImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "select_photo_empty"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSelectPhoto)))
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    lazy var selectPhotoButton: UIButton = {
        let sb = UIButton(type: .system)
        sb.setTitle("Select Photo", for: .normal)
        sb.setTitleColor(UIColor.darkBlue, for: .normal)
        sb.translatesAutoresizingMaskIntoConstraints = false
        sb.layer.cornerRadius = 10
        sb.layer.borderWidth = 1
        sb.layer.borderColor = UIColor.darkBlue.cgColor
        sb.addTarget(self, action: #selector(handleSelectPhoto), for: .touchUpInside)
        return sb
    }()
    
    @objc private func handleSelectPhoto() {
        print("try to pik an image....")
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        openPickerController(with: imagePickerController, sourceRect: UIButton())
    }
    
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let nameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let datePicker: UIDatePicker = {
        let dp = UIDatePicker()
        dp.datePickerMode = .date
        dp.translatesAutoresizingMaskIntoConstraints = false
        return dp
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        navigationItem.title = !presenter.is_update ? "Create Company" : "Edit Company"
        addLeftBarButton(title: "Cancel", selector: #selector(handleCancel))
        addRightBarButton(title: "Save", selector: #selector(handleSave))
        
        view.backgroundColor = .darkBlue
        presenter.viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    private func setupUI() {
        let lightBlueBackgroundView = setupLightBlueBackgroundView(height: 400)
        // company image view x, y, width, height.......
        view.addSubview(companyImageView)
        companyImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8).isActive = true
        companyImageView.centerXAnchor.constraint(equalTo: lightBlueBackgroundView.centerXAnchor).isActive = true
        companyImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        companyImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        // selct photo button  x, y, width, height.......
        view.addSubview(selectPhotoButton)
        selectPhotoButton.topAnchor.constraint(equalTo: companyImageView.bottomAnchor, constant: 8).isActive = true
        selectPhotoButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        selectPhotoButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        selectPhotoButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        // name label x, y, width, height.......
        view.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: selectPhotoButton.bottomAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // name text field x, y, width, height.......
        view.addSubview(nameTextField)
        nameTextField.topAnchor.constraint(equalTo: selectPhotoButton.bottomAnchor).isActive = true
        nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        nameTextField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true

        // setup date picker here.....
        view.addSubview(datePicker)
        datePicker.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive = true
        datePicker.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        datePicker.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        datePicker.bottomAnchor.constraint(equalTo: lightBlueBackgroundView.bottomAnchor).isActive = true
    }
    
    @objc private func handleSave() {
        !presenter.is_update ? createCompany() : editCompany()
    }
    
    private func createCompany() {
        guard let name = self.nameTextField.text, !name.isEmpty else { return }
        if let image = companyImageView.image {
            presenter.createCompany(photo: image, date: datePicker.date, name: name)
        }
       
    }

    private func editCompany() {
        presenter.editCompany(photo: companyImageView.image, name: nameTextField.text, date: datePicker.date)
    }
}
extension CreateCompanyController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if let EditedImage = info[.editedImage] as? UIImage {
                self.companyImageView.image = EditedImage
            }
            if let originalImage = info[.originalImage] as? UIImage {
                self.companyImageView.image = originalImage
            }
            self.setupCircularCompanyImageView()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

extension  CreateCompanyController: CompaniesView {
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    
    func create_edit_company_success() {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    func update_ui(company: Company?) {
        nameTextField.text = company?.fullName
        if let data = company?.imageData {
            companyImageView.image = UIImage(data: data)
            setupCircularCompanyImageView()
        }
        
        guard let founded = company?.founded else { return }
        datePicker.date = founded

    }
}
