//
//  CompaniesController+UITableView.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit

extension CompaniesController {
    //MARK:- table view functions...
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let company = companies[indexPath.row]
        let employeesController = EmployeesController()
        employeesController.company = company
        navigationController?.pushViewController(employeesController, animated: true)
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = .lightBlue
        return headerView
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = "No companies available...."
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.textColor = .white
        return label
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return companies.count == 0 ? 150 : 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! CompanyCell
        let company = companies[indexPath.row]
        cell.company = company
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteSwipeAction = UIContextualAction(style: .destructive, title: "Delete") { (action, _, completion) in
            self.deleteComapny(indexPath: indexPath)
            // this the last parmaeter to tell the action to start execute....
            completion(true)
        }
        //delete the the action style
        deleteSwipeAction.image = UIImage(named: "trash")
        deleteSwipeAction.backgroundColor = UIColor.lightRed
        let editSwipeAction = UIContextualAction(style: .normal, title: "Edit") { (action, _, completionHandler) in
            let company = self.companies[indexPath.row]
            self.editCompany(company: company)
            completionHandler(true)
        }
        //edit the the action style
        editSwipeAction.backgroundColor = UIColor.darkBlue
        editSwipeAction.image = UIImage(named: "Edit")
        return UISwipeActionsConfiguration(actions: [deleteSwipeAction, editSwipeAction])
    }
    
    // MARK:- company operations function....
    private func deleteComapny(indexPath: IndexPath) {
        let company = companies[indexPath.row] // hold referece for company,,,,,,
        companies.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
        // delete company from core data...
        CoreDataStack.shared.mainContext.delete(company)
        CoreDataStack.shared.saveMainContext()
    }
    private func editCompany(company: Company) {
        let editCompanyController = CreateCompanyController()
//        editCompanyController.company = company
//        editCompanyController.delegate = self
        let navController = UINavigationController(rootViewController: editCompanyController)
        present(navController, animated: true)
    }
}
