//
//  CompaniesController.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit
import CoreData

class CompaniesController: UITableViewController {
    
    // Ex func
    @objc private func handleUpdate() {
        print("Update func")
        
        // create companies
        //        CoreDataStack.shared.storeContainer.performBackgroundTask { (backgroundContext) in
        //            (0...1000).forEach({ (value) in
        //                let company = Company(context: backgroundContext)
        //                company.name = String(value)
        //            })
        //
        //            do {
        //                try backgroundContext.save()
        //                DispatchQueue.main.async {
        //                    self.companies = DataHandler.shared.fetchCompanies()
        //                    self.tableView.reloadData()
        //                }
        //            } catch let error {
        //                print(error.localizedDescription)
        //            }
        //        }
        
        //        ---------------------------------------------
        
        
        // update companies....
        //        CoreDataStack.shared.storeContainer.performBackgroundTask { (backgroundContext) in
        //            let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
        //
        //            do {
        //               let companies = try backgroundContext.fetch(fetchRequest)
        //                companies.forEach {
        //                    $0.name = "AV \($0.name ?? "")"
        //                }
        //
        //                try backgroundContext.save()
        //                DispatchQueue.main.async {
        //                    // this will forget all object and will refetch them again....
        ////                    CoreDataStack.shared.mainContext.reset()
        //                    // we don't need this..... refatch all data we need to update 2 element only....
        //                    // so we need to merge background with parent
        //                    backgroundContext.automaticallyMergesChangesFromParent = true
        //
        //                    self.companies = DataHandler.shared.fetchCompanies()
        //                    self.tableView.reloadData()
        //                }
        //            } catch let error {
        //                print(error.localizedDescription)
        //            }
        //        }
        
        
        //        ---------------------------------------------
        
        
        // final step to update correctly.....
        DispatchQueue.global(qos: .background).async {
            let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
            fetchRequest.fetchLimit = 2
            do {
                let companies = try CoreDataStack.shared.masterContext.fetch(fetchRequest)
                companies.forEach {
                    $0.fullName = "last \($0.fullName ?? "")"
                }
                do {
                    try CoreDataStack.shared.masterContext.save()
                    // until here changes didn't puched to core data yet but still on main coontext
                } catch let privateError {
                    print(privateError)
                }
                DispatchQueue.main.async {
                    // here we save into core data
                    CoreDataStack.shared.saveMainContext()
                    self.tableView.reloadData()
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    var companies = [Company]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title  = "Companies"
        addRightBarButton(with: #selector(handleAddCompany))
//        addLeftBarButton(title: "Reset", selector: #selector(handleReset))
        addLeftBarButtons(titles: ["reset", "update"], selector: [#selector(handleReset), #selector(handleUpdate)])
        tableView.register(CompanyCell.self, forCellReuseIdentifier: "cellId")
        tableView.backgroundColor = .darkBlue
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .white
        companies = DataHandler.shared.fetchCompanies()
        
    }
   
    @objc private func handleReset() {
        // delete here
        DataHandler.shared.removeCompanies()
        var indexPathsToDelete = [IndexPath]()
        for (index, _) in companies.enumerated() {
            indexPathsToDelete.append(IndexPath(row: index, section: 0))
        }
        companies.removeAll()
        tableView.deleteRows(at: indexPathsToDelete, with: .left)
    }
    
    
    @objc func handleAddCompany() {
        let createCompanyController = CreateCompanyController()
        let navController = UINavigationController(rootViewController: createCompanyController)
//        createCompanyController.delegate = self
        self.present(navController, animated: true)
    }
    
}

