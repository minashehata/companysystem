//
//  CompaniesController+CreateCompanyDelegate.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit

extension CompaniesController: CreateCompanyControllerDelegate {
    func didAddCompany(company: Company) {
        self.companies.append(company)
        // add company to table view
        let newIndexPath = IndexPath(row: self.companies.count - 1, section: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [newIndexPath], with: .automatic)
        tableView.endUpdates()
    }
    func didEditCompany(company: Company) {
        let index = companies.index(of: company)
        let indexPath = IndexPath(row: index!, section: 0)
        tableView.beginUpdates()
        tableView.reloadRows(at: [indexPath], with: .middle)
        tableView.endUpdates()
    }
}
