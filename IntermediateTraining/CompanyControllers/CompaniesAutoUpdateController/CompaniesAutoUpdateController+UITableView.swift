//
//  CompaniesAutoUpdateController+UITableView.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit
import CoreData

extension CompaniesAutoUpdateController {
    // comment this if  you don't want sections
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let label = IndentLabel()
//        label.text = fetchResultController.sections?[section].indexTitle // character
//        label.backgroundColor = UIColor.lightText
//        return label
//    }
//    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, sectionIndexTitleForSectionName sectionName: String) -> String? {
//        return sectionName
//    }
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 40
//    }
    //
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchResultController.sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchResultController.sections![section].numberOfObjects
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CompanyCell
        let company = fetchResultController.object(at: indexPath)
        cell.company = company
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let employeesController = EmployeesController()
        employeesController.company = fetchResultController.object(at: indexPath)
        navigationController?.pushViewController(employeesController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteSwipeAction = UIContextualAction(style: .destructive, title: "Delete") { (action, _, completion) in
            self.deleteComapny(indexPath: indexPath)
            // this the last parmaeter to tell the action to start execute....
            completion(true)
        }
        //delete the the action style
        deleteSwipeAction.image = UIImage(named: "trash")
        deleteSwipeAction.backgroundColor = UIColor.lightRed
        let editSwipeAction = UIContextualAction(style: .normal, title: "Edit") { [weak self] (action, _, completionHandler) in
            let company = self?.fetchResultController.object(at: indexPath)
            self?.present(Router.openCompanyViewController(company: company), animated: true)
            completionHandler(true)
        }
        //edit the the action style
        editSwipeAction.backgroundColor = UIColor.darkBlue
        editSwipeAction.image = UIImage(named: "Edit")
        return UISwipeActionsConfiguration(actions: [deleteSwipeAction, editSwipeAction])
    }
    
    // MARK:- company operations function....
    private func deleteComapny(indexPath: IndexPath) {
        // delete company from core data..
        let company = fetchResultController.object(at: indexPath)
        CoreDataStack.shared.mainContext.delete(company)
        CoreDataStack.shared.saveMainContext()
    
    }
}

