//
//  CompaniesAutoUpdateController.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit
import CoreData

class CompaniesAutoUpdateController: UITableViewController {
    // variables
    let cellId = "cellId"
    var presenter: CompaniesPresenter!
    
    
    // outlets...
    lazy var refresher: UIRefreshControl = {
        let refControl = UIRefreshControl()
        refControl.tintColor = .white
        refControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        return refControl
    }()
    
    lazy var fetchResultController: NSFetchedResultsController<Company> = {
        let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
        fetchRequest.sortDescriptors = [
            NSSortDescriptor(key: "fullName", ascending: true)
        ]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStack.shared.mainContext, sectionNameKeyPath: "fullName", cacheName: nil)
        frc.delegate = self
        do {
            try frc.performFetch()
        } catch let fetchError {
            print(fetchError)
        }
        return frc
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title  = "Companies"
        addRightBarButton(with: #selector(handleAddCompany))
        addLeftBarButton(title: "Reset", selector: #selector(handleReset))
        tableView.register(CompanyCell.self, forCellReuseIdentifier: cellId)
        tableView.backgroundColor = .darkBlue
        tableView.tableFooterView = UIView()
        tableView.separatorColor = .white
        self.refreshControl = refresher
        
    }
    @objc private func handleRefresh() {
        refreshControl?.endRefreshing()
        presenter.companies()
    }
    @objc private func handleReset() {
        let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
        let companies = try? CoreDataStack.shared.mainContext.fetch(fetchRequest)
        companies?.forEach {
            CoreDataStack.shared.mainContext.delete($0)
        }
        CoreDataStack.shared.saveMainContext()
        // or batch delete request with normal tableview delegate...
        //  presenter.deleteAllCompanies()
        
    }
    
    @objc func handleAddCompany() {
        Router.openCompanyViewController(company: nil)
    }
    
  
}


extension CompaniesAutoUpdateController: CompanyMainView {
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    
    
    func load_data_success() {
        presenter.startCashResult()
    }
    
    func load_data_error(msg: String) {
        // todo show error
    }
    
}
