//
//  CompanyService.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 08/04/2021.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import Foundation

let baseURL = "https://api.letsbuildthatapp.com/intermediate_training/companies"

struct CompanyService {
    
    func startService(completion: @escaping (_ companies: [CompanyDTO]?, _ error: String?) -> ()) {
        guard let url = URL(string: baseURL) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let data = data else { return }
            do {
                let companies_data =  try JSONDecoder().decode([CompanyDTO].self, from: data)
                completion(companies_data, nil)
            } catch let jsonError {
                completion(nil, jsonError.localizedDescription)
            }
            
        }.resume()

    }
}
