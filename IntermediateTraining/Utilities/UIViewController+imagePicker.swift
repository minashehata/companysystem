//
//  UIImagePickerController+extension.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import AVKit
import MobileCoreServices

extension UIViewController {
    
    func openPickerController(with imagePicker: UIImagePickerController, sourceRect: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let cameraAction = UIAlertAction(title: "Use Camera", style: .default) { (action) in
                let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
                if (status == .authorized) {
                    self.displayPicker(imagePicker: imagePicker, of: .camera, mediaTypes: [kUTTypeImage as String])
                }
                if (status == .restricted) {
                    self.handleRestricted(sourceRect: sourceRect)
                }
                if (status == .denied) {
                    self.handleDenied(sourceRect: sourceRect)
                }
                if (status == .notDetermined) {
                    AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted) in
                        if granted {
                            self.displayPicker(imagePicker: imagePicker, of: .camera, mediaTypes: [kUTTypeImage as String])
                        }
                    })
                }
            }
            alertController.addAction(cameraAction)
        }
        if (UIImagePickerController.isSourceTypeAvailable(.photoLibrary)) {
            let photoLibraryAction = UIAlertAction(title: "Use Photo Library", style: .default) { (action) in
                let status = PHPhotoLibrary.authorizationStatus()
                if (status == .authorized) {
                    self.displayPicker(imagePicker: imagePicker, of: .photoLibrary, mediaTypes: [kUTTypeImage as String])
                }
                if (status == .restricted) {
                    self.handleRestricted(sourceRect: sourceRect)
                }
                if (status == .denied) {
                    self.handleDenied(sourceRect: sourceRect)
                }
                if (status == .notDetermined) {
                    PHPhotoLibrary.requestAuthorization({ (status) in
                        if status == PHAuthorizationStatus.authorized
                        {
                            self.displayPicker(imagePicker: imagePicker, of: .photoLibrary, mediaTypes: [kUTTypeImage as String])
                        }
                    })
                }
            }
            alertController.addAction(photoLibraryAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.popoverPresentationController?.sourceView = sourceRect
        alertController.popoverPresentationController?.sourceRect = sourceRect.bounds
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    
    func handleDenied(sourceRect: UIButton) {
        let alertController = UIAlertController(title: "Media Access Denied", message: "trips doesn't have access to use your device's media. please update you settings", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Go To Settings", style: .default) { (action) in
            DispatchQueue.main.async {
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.popoverPresentationController?.sourceRect = sourceRect.bounds
        alertController.popoverPresentationController?.sourceView = sourceRect
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    func handleRestricted(sourceRect: UIButton) {
        let alertController = UIAlertController(title: "Media Access Denied", message: "This device is restricted from accessing any media at this time", preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.popoverPresentationController?.sourceRect = sourceRect.bounds
        alertController.popoverPresentationController?.sourceView = sourceRect
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
    func displayPicker(imagePicker: UIImagePickerController, of type: UIImagePickerController.SourceType, mediaTypes: [String]) {
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: type)!
        imagePicker.mediaTypes = mediaTypes // the default value is kUTTypeImage ..
        imagePicker.sourceType = type
        imagePicker.allowsEditing = true
        
        DispatchQueue.main.async {
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}
