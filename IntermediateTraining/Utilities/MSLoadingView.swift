//
//  MSLoadingView.swift
//  Template-iOS
//
//  Created by Mina Shehata on 1/3/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import UIKit
import AVFoundation

@objc protocol SpinnerView {
    
    func ShowSpinner()
    func HideSpinner()
    
}

class MSLoadingView: UIViewXRadialGradient {


    
    let blackView: UIVisualEffectViewX = {
        let bv = UIVisualEffectViewX()
        bv.backgroundColor = UIColor.lightRed
        bv.alpha = 0.8
        bv.cornerRadius = 15
        bv.shadowColor = .black
        bv.shadowOffset = CGSize(width: 0.5, height: 0.5)
        bv.shadowRadius = 4
        bv.shadowOpacity = 0.5
        return bv
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let av = UIActivityIndicatorView(style: .whiteLarge)
        av.color = .white
        av.hidesWhenStopped = true
        return av
    }()
    
    let upload_Label: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 25, weight: .medium)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        backgroundColor = UIColor.darkBlue
//        InsideColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
//        OutsideColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2643407534)
        upload_Label.text = nil
        
        addSubview(blackView)
        blackView.anchorSize(to: self, size: CGSize(width: 100, height: 100), center: true)
        
        blackView.contentView.addSubview(activityIndicator)
        activityIndicator.anchorSize(to: blackView.contentView, size: .zero, center: true)
        addSubview(upload_Label)
        upload_Label.anchor(top: blackView.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 16, left: 0, bottom: 0, right: 0), centerX: true, toView: blackView)
    }
    
    
    class func addLoadingImageOn(view: UIView) {
        
        let imageView = UIImageView()
        view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        // setypAnimation.......
        
        var arrayOfImages = [UIImage]()
        for i in 0...29 {
            if let img = UIImage(named: "\(i)") {
                arrayOfImages.append(img)
            }
        }
        // start animation loading.......

        imageView.animationImages = arrayOfImages
        imageView.animationDuration = 1.1
        imageView.animationRepeatCount = 0 // infinite
        imageView.startAnimating()
//
    }
    
}


extension UIViewController {
    
    public func StartLoading() {
        DispatchQueue.main.async { [weak self] in
            guard let window = UIApplication.shared.windows.first else { return }
            let view = MSLoadingView(frame: window.frame)
            window.addSubview(view)
//            MSLoadingView.addLoadingImageOn(view: view.blackView.contentView)
            view.activityIndicator.startAnimating()
            Router.transition(with: window)
        }
    }
    
    public func StopLoading() {
        DispatchQueue.main.async { [weak self] in
            guard let window = UIApplication.shared.windows.first else { return }
            window.subviews.forEach {
                if let msLoaderView = $0 as? MSLoadingView {
                    msLoaderView.activityIndicator.stopAnimating()
                    msLoaderView.removeFromSuperview()
                }
            }
            Router.transition(with: window)
        }
    }
    
}




extension UIView {
    
    @discardableResult
    public func fillSuperView(padding: UIEdgeInsets = .zero) -> [NSLayoutConstraint?] {
        return anchor(top: superview?.topAnchor, leading: superview?.leadingAnchor, bottom: superview?.bottomAnchor, trailing: superview?.trailingAnchor)
    }
    
    public func anchorSize(to view: UIView, size: CGSize = .zero, center: Bool = false, xConstant: CGFloat = 0, yConstant: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if size != CGSize.zero {
            widthAnchor.constraint(equalToConstant: size.width).isActive = true
            heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
        if center {
            centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: xConstant).isActive = true
            centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: yConstant).isActive = true
        }
    }
    
    @discardableResult
    public func anchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero, centerX: Bool = false, centerY: Bool = false, toView: UIView = UIView()) -> [NSLayoutConstraint?] {
        translatesAutoresizingMaskIntoConstraints = false
        var topanchor: NSLayoutConstraint?
        var leadinganchor: NSLayoutConstraint?
        var trailinganchor: NSLayoutConstraint?
        var bottomanchor: NSLayoutConstraint?
        
        var widthanchor: NSLayoutConstraint?
        var heightanchor: NSLayoutConstraint?

        if let top = top { topanchor = topAnchor.constraint(equalTo: top, constant: padding.top)
            topanchor?.isActive = true }
        if let leading = leading { leadinganchor = leadingAnchor.constraint(equalTo: leading, constant: padding.left)
            leadinganchor?.isActive = true }
        if let bottom = bottom { bottomanchor = bottomAnchor.constraint(equalTo: bottom, constant: -padding.bottom)
            bottomanchor?.isActive = true }
        if let trailing = trailing { trailinganchor = trailingAnchor.constraint(equalTo: trailing, constant: -padding.right)
            trailinganchor?.isActive = true }
        
        if size.width != 0 { widthanchor = widthAnchor.constraint(equalToConstant: size.width)
            widthanchor?.isActive = true }
        if size.height != 0 { heightanchor = heightAnchor.constraint(equalToConstant: size.height)
            heightanchor?.isActive = true }
        
        if centerX { centerXAnchor.constraint(equalTo: toView.centerXAnchor).isActive = true }
         if centerY { centerYAnchor.constraint(equalTo: toView.centerYAnchor).isActive = true }
        
        return [topanchor, leadinganchor, bottomanchor, trailinganchor, widthanchor, heightanchor]
    }
    
    public func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}

