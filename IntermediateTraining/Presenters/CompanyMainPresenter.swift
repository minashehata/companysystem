//
//  CompanyMainPresenter.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 07/04/2021.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import Foundation

@objc protocol CompanyMainView: class, SpinnerView {
    @objc optional func load_data_success()
    @objc optional func load_data_error(msg: String)
}
class CompanyMainPresenter: NSObject {
    
    private weak var view: CompanyMainView?
    
    init(view: CompanyMainView) {
        self.view = view
        super.init()
    }
    
    // TODO :- method here add features in company module...
}
