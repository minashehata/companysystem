//
//  CompaniesPresenter.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 07/04/2021.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit

protocol CompaniesView: CompanyMainView {
    func update_ui(company: Company?)
    func create_edit_company_success()
}
final class CompaniesPresenter: CompanyMainPresenter {
    private weak var view: CompanyMainView?
    private var companies_data: [CompanyDTO]
    private var company: Company?
    private let service = CompanyService()
    init(view: CompanyMainView, company: Company? = nil) {
        self.view = view
        self.company = company
        self.companies_data = []
        super.init(view: view)
    }
    
    func companies() {
        view?.ShowSpinner()
        service.startService { [weak self] (companies, error) in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let companies = companies {
                self.companies_data = companies
                self.view?.load_data_success?()
            } else{
                self.view?.load_data_error?(msg: error ?? "")
            }
        }
    }

    func startCashResult() {
        DataHandler.shared.createCompaniesFrom(JSONCompanies: self.companies_data)
    }
    
    //  for normal way
    func deleteAllCompanies() {
        DataHandler.shared.removeCompanies()
    }
    
    // MARK:- create edit company presenter
    var is_update: Bool {
        return company != nil
    }
    func viewDidLoad() {
        (view as? CompaniesView)?.update_ui(company: company)
    }
    func editCompany(photo: UIImage?, name: String?, date: Date?) {
        company?.fullName = name
        company?.founded = date
        if let image = photo {
            let imageData = image.jpegData(compressionQuality: 0.8)
            company?.imageData = imageData
        }
        
        CoreDataStack.shared.saveMainContext()
        (view as? CompaniesView)?.create_edit_company_success()
    }
    func createCompany(photo: UIImage, date: Date, name: String) {
        _ = DataHandler.shared.createCompany(with: name, date: date, image: photo)
        (view as? CompaniesView)?.create_edit_company_success()
    }
}
