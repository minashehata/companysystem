//
//  DTO's.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import Foundation

struct CompanyDTO: Decodable {
    var name: String?
    var photoUrl: String?
    var founded: String?
    var employees: [EmployeeDTO]?
    enum CodingKey: String {
        case name = "name"
        case founded = "founded"
        case photoUrl = "photoUrl"
    }
}

struct EmployeeDTO: Decodable {
    var name: String?
    var birthday: String?
    var type: String?
    var company: CompanyDTO?
    enum CodingKey: String {
        case name = "name"
        case birthday = "birthday"
        case type = "type"
    }
}
