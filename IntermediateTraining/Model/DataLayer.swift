//
//  Company.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit
import CoreData

final class DataHandler {
    
    static let shared = DataHandler()
    private init() { }
    
    // MARK:- companies operation
    func fetchCompanies() -> [Company] {
        let fetchRequest: NSFetchRequest<Company> = Company.fetchRequest()
        do {
            let companies = try CoreDataStack.shared.mainContext.fetch(fetchRequest)
            return companies
        } catch let fetchError {
            print("failed to fetch companes:", fetchError)
            return []
        }
    }
    
    func removeCompanies() {
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Company.fetchRequest())
        do {
            try CoreDataStack.shared.mainContext.execute(batchDeleteRequest)
        }  catch let deleteError {
            print("Failed to batch Delete: ", deleteError)
        }
        CoreDataStack.shared.saveMainContext()
    }
    
    func createCompany(with name: String, date: Date, image: UIImage) -> Company {
        let company = Company(context: CoreDataStack.shared.mainContext)
        company.fullName = name
        company.founded = date
        let imageData = image.jpegData(compressionQuality: 0.8)
        company.imageData = imageData
        CoreDataStack.shared.saveMainContext()
        return company
    }
    // MARK:- end company operations.....
    
    
    // MARK:- Employee operations......
    func createEmployee(name: String, employeeType: String, company: Company, birthdayDate: Date) -> Employee {
        let employee = Employee(context: CoreDataStack.shared.mainContext)
        employee.name = name
        employee.company = company
        employee.birthday = birthdayDate
        employee.type = employeeType
        CoreDataStack.shared.saveMainContext()
        return employee
    }
   
    
    // MARK:- creating core data objects from json
    
    func createCompaniesFrom(JSONCompanies: [CompanyDTO]) {
        JSONCompanies.forEach {
            let company = Company(context: CoreDataStack.shared.masterContext)
            company.fullName = $0.name
            company.imageURL = $0.photoUrl
            company.founded = $0.founded?.getDateFromString()
            $0.employees?.forEach({ (jsonEmployee) in
                let employee = Employee(context: CoreDataStack.shared.masterContext)
                employee.name = jsonEmployee.name
                employee.birthday = jsonEmployee.birthday?.getDateFromString()
                employee.company = company
                employee.type = jsonEmployee.type
            })
        }
        do {
            try CoreDataStack.shared.masterContext.save()
            try CoreDataStack.shared.masterContext.parent?.save()
        } catch let saveError {
            print("Failed to save objects in core data", saveError)
        }
    }
    
}
