//
//  CreateEmployeeController.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit

protocol CreateEmployeeControllerDelegate {
    func didAddEmployee(employee: Employee)
}

class CreateEmployeeController: UIViewController {
    
    var delegate: CreateEmployeeControllerDelegate?
    var company: Company?
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Name"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let nameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let birthdayLabel: UILabel = {
        let label = UILabel()
        label.text = "Birthday"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let birthdayTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "MM/dd/yyyy"
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    let employeeTypeSegmentControl: UISegmentedControl = {
//        let items = ["Executive", "Senior Managment", "Staff"]
        let items = [
            EmployeeType.Executive.rawValue,
            EmployeeType.SeniorManagement.rawValue,
            EmployeeType.Staff.rawValue,
            EmployeeType.Intern.rawValue
        ]
        let sc = UISegmentedControl(items: items)
        sc.translatesAutoresizingMaskIntoConstraints = false
        sc.tintColor = .darkBlue
        sc.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 15)], for: .normal)
        sc.selectedSegmentIndex = 0
        return sc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Create Employee"
        view.backgroundColor = .darkBlue
        addLeftBarButton(title: "Cancel", selector: #selector(handleCancel))
        addRightBarButton(title: "Save", selector: #selector(handleSaveEmployee))
        setupUI()
    }
    
    private func setupUI() {
        _ = setupLightBlueBackgroundView(height: 150)
        // name label x, y, width, height.......
        view.addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        nameLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // name text field x, y, width, height.......
        view.addSubview(nameTextField)
        nameTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        nameTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        nameTextField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        nameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // birthday label x, y, width, height.......
        view.addSubview(birthdayLabel)
        birthdayLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        birthdayLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        birthdayLabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        birthdayLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // birthday text field x, y, width, height.......
        view.addSubview(birthdayTextField)
        birthdayTextField.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive = true
        birthdayTextField.leftAnchor.constraint(equalTo: nameLabel.rightAnchor).isActive = true
        birthdayTextField.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        birthdayTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        // segment control x, y, width, height.......
        view.addSubview(employeeTypeSegmentControl)
        employeeTypeSegmentControl.topAnchor.constraint(equalTo: birthdayTextField.bottomAnchor).isActive = true
        employeeTypeSegmentControl.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16).isActive = true
        employeeTypeSegmentControl.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16).isActive = true
        employeeTypeSegmentControl.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    @objc private func handleSaveEmployee() {
        guard let name = nameTextField.text, !name.isEmpty else { return }
        guard let company = company else { return }
        
        guard let birthdayText = birthdayTextField.text, !birthdayText.isEmpty else {
            showAlert(title: "Empty Birthday", message: "You have not entered a birthday.")
            return
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC") // cairo time zone.......
        guard let birthdayDate = dateFormatter.date(from: birthdayText) else {
            showAlert(title: "Not valid", message: "The birthday you entered is not match the correct format.")
            return
        }
        guard let employeeType = employeeTypeSegmentControl.titleForSegment(at: employeeTypeSegmentControl.selectedSegmentIndex) else { return }
        
        let employee = DataHandler.shared.createEmployee(name: name, employeeType: employeeType, company: company, birthdayDate: birthdayDate)
        dismiss(animated: true) {
            print("Successfullty")
            self.delegate?.didAddEmployee(employee: employee)
            // do delegation here
        }
    }
    
}
