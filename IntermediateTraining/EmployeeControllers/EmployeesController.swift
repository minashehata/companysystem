//
//  EmployeesController.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import UIKit

class EmployeesController: UITableViewController, CreateEmployeeControllerDelegate {
    func didAddEmployee(employee: Employee) {
        
        guard let section = employeeTypes.index(of: employee.type!) else { return }
        
        let row = employees[section].count
        let newInsertionIndexPath = [IndexPath(row: row, section: section)]
        employees[section].append(employee)
        
        tableView.beginUpdates()
        tableView.insertRows(at: newInsertionIndexPath, with: .fade)
        tableView.endUpdates()
    }
    
    var company: Company?
    
    var employees = [[Employee]]()
    var employeeTypes = [
        EmployeeType.Executive.rawValue,
        EmployeeType.SeniorManagement.rawValue,
        EmployeeType.Staff.rawValue,
        EmployeeType.Intern.rawValue
    ]
    fileprivate let cellId = "cellId"
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = company?.fullName
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = .darkBlue
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        addRightBarButton(with: #selector(handleAddEmpoyee))
        fetchEmployees()
    }
    
    private func fetchEmployees() {
        guard let allemployees = company?.employees?.allObjects as? [Employee] else { return }
        employees = []
        employeeTypes.forEach { (emType) in
            employees.append(allemployees.filter { $0.type == emType })
        }
    }
    @objc private func handleAddEmpoyee() {
        let createEmployeeController = CreateEmployeeController()
        createEmployeeController.delegate = self
        createEmployeeController.company = company
        let navgationController = UINavigationController(rootViewController: createEmployeeController)
        present(navgationController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        let employee = employees[indexPath.section][indexPath.row]
        
        if let name = employee.name, let date = employee.birthday {
            print("xxx", convertDate(date: date))
            cell.textLabel?.text = name + " - Birthday: " +  convertDate(date: date)
        }
        else {
            cell.textLabel?.text = employee.name
        }
        
        cell.backgroundColor = .tealColor
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        cell.textLabel?.textColor = .white
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employees[section].count
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let customlabel = IndentLabel()
        customlabel.text = employeeTypes[section]
        customlabel.backgroundColor = .lightBlue
        return customlabel
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return employeeTypes.count
    }
}
