//
//  EmployeeType.swift
//  IntermediateTraining
//
//  Created by Mina Shehata on 03/29/21.
//  Copyright © 2021 Mina Shehata. All rights reserved.
//

import Foundation

enum EmployeeType: String {
    case Executive
    case SeniorManagement = "Senior Management"
    case Staff
    case Intern
}
